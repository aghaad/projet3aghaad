<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon -->
    <link rel="icon" href="public/images/favicon.ico">

    <title><?= $title ?></title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="public/css/bootstrap-paper.css">

    <!-- Style général de la page -->
    <link rel="stylesheet" href="public/css/style.css">

    <!-- FontAwesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">

    <!-- Tinymce -->
    <script src="public/js/tinymce/tinymce.min.js"></script>
    <script>
        tinymce.init({
            mode : "textareas",
            editor_selector : "mceEditor"
        });
    </script>
</head>

<body>
    <!-- Wrapper -->
            <div id="wrapper">

<div class="container-fluid">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <!-- Hamburger -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php" title="Accueil" >Billet simple pour l'Alaska</a>
            </div>
            <!-- Menu -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">
                    <li><a href="?controller=PostController&action=indexAction" title="Lire les anciens billets">Liste des chapitres</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php
                    if(isset($_SESSION) && empty($_SESSION)) {
                        ?>
                        <li><a href="?controller=UserController&action=loginAction" title="Se connecter">Connexion</a></li>
                        <?php
                    }
                    ?>
                    <?php
                    if(isset($_SESSION) && !empty($_SESSION))
                    {
                        ?>
                        <!--<li><a href="?controller=UserController&action=registerAction" title="S'incrire">Inscription</a></li>-->
                        <li><a href="?controller=AdminController&action=indexAction" title="Espace d'administration">Administration</a></li>
                        <li><a href="?controller=UserController&action=logoutAction" title="Se déconnecter">Déconnexion</a></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>
</div>

<div class="container">

    <!-- Messages d'alerte -->
    <?php include 'inc/_alertMessage.php'; ?>

    <!-- Contenu de la page -->
    <?= $content ?>

</div><!-- /.container -->
</div><!-- wrapper -->
<footer class="footer">
    <!--Copyright-->
    <div class="footer-copyright py-3 text-center">
        <strong>© 2018 Copyright : <a href="?controller=PostController&action=about" title="À propos de l'auteur">Jean Forteroche</a></strong>
    </div>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
